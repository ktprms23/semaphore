#include <stdio.h>
#include <semaphore.h>
#include <fcntl.h>

#include "common.h"


int main(void) {


	sem_t * mutex, * mutex2;

	if ((mutex = sem_open(SEM_NAME, O_RDWR, 0644)) == SEM_FAILED) {
		printf( "ERROR: failed to load semaphore\n" );
		return -1;
	} // end if
/*
	if ((mutex2 = sem_open(SEM_NAME_2, O_RDWR, 0644)) == SEM_FAILED) {
		printf( "ERROR: failed to create semaphore 2\n" );
		//printf( "%s\n", sem_destroy(mutex) == 0 ? "Destory DONE." : "Destory FAIL" );
		return -1;
	} // end if
*/

//	sem_wait(mutex2);
	//testV = 2;
//	sem_post(mutex2);
	sem_post(mutex);

	
	return 0;

} // end main
