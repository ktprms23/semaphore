#include <stdio.h>
#include <semaphore.h>
#include <fcntl.h>
#include "common.h"

int main(void) {

	sem_t * mutex, * mutex2;


	if ((mutex = sem_open(SEM_NAME, O_CREAT, 0644, 0)) == SEM_FAILED) {
		printf( "ERROR: failed to create semaphore\n" );
		return -1;
	} // end if
/*
	if ((mutex2 = sem_open(SEM_NAME_2, O_CREAT, 0644, 1)) == SEM_FAILED) {
		printf( "ERROR: failed to create semaphore 2\n" );
		printf( "%s\n", sem_destroy(mutex) == 0 ? "Destory DONE." : "Destory FAIL" );
		return -1;
	} // end if
*/

	printf( "MAIN: start while loop\n" );
	
	while(1) {
		sem_wait(mutex);
		printf( "MAIN: pass wait\n" );
//		printf( "testV = %d\n", testV );
//		if( testV == 500 ) {
			
//			break;
//		} // end if


		printf( "MAIN: keep waiting\n" );
		//sem_post(mutex);
	} // end while

	printf( "%s\n", sem_destroy(mutex) == 0 ? "Destory DONE." : "Destory FAIL" );
//	printf( "%s\n", sem_destroy(mutex2) == 0 ? "Destory 2 DONE." : "Destory 2 FAIL" );

	return 0;
	

} // end main
